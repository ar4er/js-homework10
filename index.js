const tabs = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelector(".tabs-content");
const tabsContentChildren = tabsContent.children;

const tabsArray = [...tabs];
const tabsContentChildrenArray = [...tabsContentChildren];

function tabsContentRemove(arr) {
  arr.forEach((element) => {
    element.style.display = "none";
  });
}

function defaultState() {
  tabsContentRemove(tabsContentChildrenArray);

  tabsArray.forEach((element) => {
    element.classList.remove("active");
  });
}

defaultState();

// tabsArray.forEach((element, index) => {
//   element.addEventListener("click", () => {
//     tabsArray.forEach((element) => {
//       element.classList.remove("active");
//     });
//     tabsContentRemove(tabsContentChildrenArray);
//     element.classList.add("active");
//     tabsContentChildrenArray[index].style.display = "block";
//   });
// });

tabsArray.forEach((element) => {
  element.addEventListener("click", () => {
    tabsArray.forEach((tab) => {
      tab.classList.remove("active");
    });
    element.classList.add("active");
    tabsContentChildrenArray.forEach((el) => {
      if (element.innerText.toLowerCase() === el.dataset.title.toLowerCase()) {
        tabsContentRemove(tabsContentChildrenArray);

        el.style.display = "block";
      }
    });
  });
});
